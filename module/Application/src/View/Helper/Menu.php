<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Menu extends AbstractHelper 
{
	protected $items = [];
	protected $activeItemId = '';
	
	public function __construct($items = []) 
	{
		$this->items = $items;
	}
	
	public function setItems($items) 
	{
		$this->items = $items;
	}
	
	public function setActiveItemId($activeItemId) 
	{
		$this->activeItemId = $activeItemId;
	}
	
	public function render() 
	{
		if (count($this->items) == 0) return ''; // Do nothing if there are no items.
		
		$result = null;
		$result .= '<nav class="navbar navbar-default" role="navigation">';
		$result .= '<div class="navbar-header">';
		$result .= '<button type="button" class="navbar-toggle" data-toggle="collapse"';
		$result .= 'data-target=".navbar-ex1-collapse">';
		$result .= '<span class="sr-only">Toggle navigation</span>';
		$result .= '<span class="icon-bar"></span>';
		$result .= '<span class="icon-bar"></span>';
		$result .= '<span class="icon-bar"></span>';
		$result .= '</button>';
		$result .= '</div>';
		
		$result .= '<div class="collapse navbar-collapse navbar-ex1-collapse">';        
		$result .= '<ul class="nav navbar-nav">';
		
		foreach ($this->items as $item) { $result .= $this->renderItem($item); }
		
		$result .= '</ul>';
		$result .= '</div>';
		$result .= '</nav>';
		
		return $result;
		
	}
	
	protected function renderItem($item) 
	{
		$escape_html = $this->getView()->plugin('escapeHtml');
		$translate = $this->getView()->plugin('translate');
		
		$id = isset($item['id']) ? $item['id'] : '';
		$label = isset($item['label']) ? $translate($item['label']) : '';
		$icon = isset($item['icon']) ? $item['icon'] : '';
		
		$is_active = ($id == $this->activeItemId);
		
		$result = null; 
		
		if (isset($item['dropdown'])) {
			
			$dropdown_items = $item['dropdown'];
			
			$result .= '<li class="dropdown ' . ($is_active ? 'active' : '') . '">';
			$result .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown">';
			
			if ($icon) $result .= '<span class="glyphicon ' . $icon . '" aria-hidden="true"></span> ';
			
			$result .= $escape_html($label) . ' <b class="caret"></b>';
			$result .= '</a>';
		   
			$result .= '<ul class="dropdown-menu">';
			
			foreach ($dropdown_items as $item) {
				$link = isset($item['link']) ? $item['link'] : '#';
				$label = isset($item['label']) ? $translate($item['label']) : '';
				$icon = isset($item['icon']) ? $item['icon'] : '';
				
				$result .= '<li>';
				$result .= '<a href="' . $escape_html($link) . '">';
				
				if ($icon) $result .= '<span class="glyphicon ' . $icon . '" aria-hidden="true"></span> ';
				
				$result .= $escape_html($label) . '</a>';
				$result .= '</li>';
			}
			
			$result .= '</ul>';
			$result .= '</li>';
		} else {
			$link = isset($item['link']) ? $item['link'] : '#';
			
			$result .= $is_active ? '<li class="active">' : '<li>';
			
			$result .= '<a href="' . $escape_html($link) . '">';
			
			if ($icon) $result .= '<span class="glyphicon ' . $icon . '" aria-hidden="true"></span> ';
			
			$result .= $escape_html($label).'</a>';
			$result .= '</li>';
		}
		
		return $result;
	}
}
