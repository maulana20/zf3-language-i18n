<?php
namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;

class Module
{
	const VERSION = '3.0.0dev';
	
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}
	
	public function onBootstrap(MvcEvent $event)
	{
		$application = $event->getApplication();
		$servicemanager = $application->getServiceManager();
		$sessionmanager = $servicemanager->get(SessionManager::class);
		
		$container = $servicemanager->get('I18nSessionContainer');
		
		$language_id = 'en_US';
		
		if (isset($container->languageId)) $language_id = $container->languageId;
		
		\Locale::setDefault($language_id);
		
		$translator = $event->getApplication()->getServiceManager()->get('MvcTranslator');
		$translator->addTranslationFile(
			'phpArray',
			'./vendor/zendframework/zend-i18n-resources/languages/'.substr($language_id, 0, 2).'/Zend_Validate.php',
			'default',
			$language_id
		);
		$translator->addTranslationFilePattern(
			'phpArray',
			'./data/language',
			'%s.php',
			'default'
		);
		
		\Zend\Validator\AbstractValidator::setDefaultTranslator(new \Zend\Mvc\I18n\Translator($translator));
	}
}
