<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController 
{
	private $i18n_session;
	
	public function __construct($i18n_session) 
	{
		$this->i18n_session = $i18n_session;
	}
	
	public function indexAction() 
	{
		return new ViewModel([ 'languageId' => $this->i18n_session->languageId ]);
	}
	
	public function aboutAction() 
	{
		return new ViewModel([ 'appName' => 'i18n Demo', 'appDescription' => 'Internationalization & Localization demo for the Using Zend Framework 3 book' ]);
	}
	
	public function setLanguageAction()
	{
		$languageId = $this->params()->fromRoute('id', 'en_US');
		
		$this->i18n_session->languageId = $languageId;
		
		return $this->redirect()->toRoute('home');
	}
}
