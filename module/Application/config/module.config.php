<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;

return [
	'router' => [
		'routes' => [
			'home' => [
				'type' => Literal::class,
				'options' => [
					'route'    => '/',
					'defaults' => [
						'controller' => Controller\IndexController::class,
						'action'     => 'index',
					],
				],
			],
			'application' => [
				'type'    => Segment::class,
				'options' => [
					'route'    => '/application[/:action[/:id]]',
					'defaults' => [
						'controller'    => Controller\IndexController::class,
						'action'        => 'index',
					],
				],
			],
			'about' => [
				'type' => Literal::class,
				'options' => [
					'route'    => '/about',
					'defaults' => [
						'controller' => Controller\IndexController::class,
						'action'     => 'about',
					],
				],
			],
			'contactus' => [
				'type'    => Literal::class,
				'options' => [
					'route'    => '/contactus',
					'defaults' => [
						'controller'    => Controller\IndexController::class,
						'action'        => 'contactUs',
					],
				],
			],    
			'payment' => [
				'type'    => Literal::class,
				'options' => [
					'route'    => '/payment',
					'defaults' => [
						'controller'    => Controller\IndexController::class,
						'action'        => 'payment',
					],
				],
			],
		],
	],
	'controllers' => [
		'factories' => [
			Controller\IndexController::class => Controller\Factory\IndexControllerFactory::class,
		],
	],
	'session_containers' => [
		'I18nSessionContainer'
	],
	'view_helpers' => [
		'factories' => [
			View\Helper\Menu::class => InvokableFactory::class,
			View\Helper\Breadcrumbs::class => InvokableFactory::class,
		],
		'aliases' => [
			'mainMenu' => View\Helper\Menu::class,
			'pageBreadcrumbs' => View\Helper\Breadcrumbs::class,
		],
	],
	'view_manager' => [
		'display_not_found_reason' => true,
		'display_exceptions'       => true,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'error/404',
		'exception_template'       => 'error/index',
		'template_map' => [
			'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
			'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
			'error/404'               => __DIR__ . '/../view/error/404.phtml',
			'error/index'             => __DIR__ . '/../view/error/index.phtml',
		],
		'template_path_stack' => [
			__DIR__ . '/../view',
		],
	],
];
