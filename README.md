# zf3-language-i18n

## Getting Started

Bersumber dari https://github.com/olegkrivtsov/using-zf3-book-samples/tree/master/i18ndemo .

Kemudian ambil library zend.
```bash
composer install
```

Library yang di gunakan.
```bash
"zendframework/zend-mvc-i18n": "^1.1"
```

Running local :
```bash
php -S 0.0.0.0:8080 -t public public/index.php
```

![english](https://gitlab.com/maulana20/zf3-language-i18n/-/raw/master/screen/english.PNG)

Ubah bahasa.
![rusian](https://gitlab.com/maulana20/zf3-language-i18n/-/raw/master/screen/rusian.PNG)
